from itertools import combinations
from sys import argv

# the bast solution by number of steps
best :list=[]

def find_the_way(target :int, numbers :list, solution :list=[]):
    global best

    #internal function to do the recursion
    def go(a :int, b :int, res :int, formula :str):
        # arguments: a and b: indexes, result of operation, the string formula of the latest step
        
        global best

        new_solution = solution + [formula]
        # check if we really found a new solution
        if res == target:
            # if it's the first, or shorter than the currently known best one
            if not best or len(new_solution) < len(best):
                print(f"{len(new_solution)}: {new_solution}")
                best=new_solution
        else:
            # list references passed with assignment 
            new_numbers=numbers.copy()
            # replace the first number with the result, delete the second one
            new_numbers[a]=int(res)
            del(new_numbers[b])
            # find the result with the current, smaller list of numbers
            find_the_way(target, new_numbers, new_solution)
    
    # we consumed all numbers without reaching the target
    if len(numbers) < 2:
        return
    # iterate over all possible pair of numbers (pick two numbers from the list by index).
    for a_index,b_index in combinations(list(range(len(numbers))), 2):
        # iterate over all operators
        # for "/" and "-" we have got some extra conditions to consider
        a, b = numbers[a_index], numbers[b_index]
        go(a_index, b_index, a+b, f"{a} + {b} = {a+b}")
        go(a_index, b_index, a*b, f"{a} * {b} = {a*b}")
        if b and not a%b:
            go(a_index, b_index, int(a/b), f"{a} / {b} = {a/b}")
        elif a and not b%a:
            go(a_index, b_index, int(b/a), f"{b} / {a} = {b/a}")
        if a>b:
            go(a_index, b_index, a-b, f"{a} - {b} = {a-b}")
        elif b>a:
            go(a_index, b_index, b-a, f"{b} - {a} = {b-a}")

if __name__ == "__main__":
    if len(argv) > 2:
      t = int(argv[1])
      l = list(map(int, argv[2::]))
    else:
      t=eval(input('Enter target: '))
      l=list(eval((input('Enter numbers: '))))
    find_the_way(t, l)
