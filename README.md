# nyt-digits-solver

## simple recursive python to solve the popular (?) NYT game: [digits](https://www.nytimes.com/games/digits)

### function: find_the_way()

argument: target number, list of available numbers, optional list of steps we took this far

works: 
- tries all combination (unordered pairs) of available indexes to select numbers for operation
- tries all operators for the pair. where order counts, twice. then calls go function to make the selected step

output:
- prints the solution (with its length - number of operations), if it is better then the last known one. 
output will be like: 
```
5: ['3 + 6 = 9', '8 + 15 = 23', '23 + 20 = 43', '9 * 43 = 387', '387 - 9 = 378']
4: ['3 * 6 = 18', '9 - 8 = 1', '1 + 20 = 21', '18 * 21 = 378']
3: ['3 * 9 = 27', '6 + 8 = 14', '27 * 14 = 378']
```

### sub-function: go()
arguments: a,b: indices in the numbers list, last operations result, applied formula

works:
- checks result
- eliminates the numbers by replacing the first selected number with the result, and deleting the other
- calls find_the_way with the shorter list of numbers, and longer list of solution steps

