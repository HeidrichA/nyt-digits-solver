package main

import (
	"fmt"
	"os"
	"strconv"
)

var best []string

func findTheWay(target int, numbers []int, solution []string) {
	goFunc := func(a, b, res int, formula string) {
		newSolution := append(solution, formula)
		if res == target && (len(best) == 0 || len(newSolution) < len(best)) {
			fmt.Printf("%d: %v\n", len(newSolution), newSolution)
			best = newSolution
		} else {
			newNumbers := make([]int, len(numbers))
			copy(newNumbers, numbers)
			newNumbers[a] = res
			newNumbers = append(newNumbers[:b], newNumbers[b+1:]...)
			findTheWay(target, newNumbers, newSolution)
		}
	}

	if len(numbers) < 2 {
		return
	}

	for aIndex := 0; aIndex < len(numbers)-1; aIndex++ {
		for bIndex := aIndex + 1; bIndex < len(numbers); bIndex++ {
			a, b := numbers[aIndex], numbers[bIndex]
			goFunc(aIndex, bIndex, a+b, fmt.Sprintf("%d + %d = %d", a, b, a+b))
			goFunc(aIndex, bIndex, a*b, fmt.Sprintf("%d * %d = %d", a, b, a*b))
			if b != 0 && a%b == 0 {
				goFunc(aIndex, bIndex, a/b, fmt.Sprintf("%d / %d = %d", a, b, a/b))
			} else if a != 0 && b%a == 0 {
				goFunc(aIndex, bIndex, b/a, fmt.Sprintf("%d / %d = %d", b, a, b/a))
			}
			if a > b {
				goFunc(aIndex, bIndex, a-b, fmt.Sprintf("%d - %d = %d", a, b, a-b))
			} else if b > a {
				goFunc(aIndex, bIndex, b-a, fmt.Sprintf("%d - %d = %d", b, a, b-a))
			}
		}
	}
}

func main() {
	var target int
	var numbers []int

	if len(os.Args) > 2 {
		target, _ = strconv.Atoi(os.Args[1])
		numbers = make([]int, len(os.Args)-2)
		for i := 2; i < len(os.Args); i++ {
			numbers[i-2], _ = strconv.Atoi(os.Args[i])
		}
	} else {
		fmt.Print("Enter target: ")
		fmt.Scanf("%d", &target)
		fmt.Print("Enter numbers: ")
		for {
			var num int
			_, err := fmt.Scanf("%d", &num)
			if err != nil {
				break
			}
			numbers = append(numbers, num)
		}
	}

	findTheWay(target, numbers, []string{})
}

